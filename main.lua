require 'game.board'
require 'game.timer'
require 'game.collision'

function love.load()
	board = game.Board()
	local image = love.graphics.newImage('img/font.png')
	image:setFilter('nearest','nearest')
	local font = love.graphics.newImageFont(image, 'ADPUSCORE0123456789 ')
	love.graphics.setFont(font)
end

function love.draw()
	love.graphics.scale(4,4)
	love.graphics.setLineStyle('rough')
	love.graphics.setLineWidth(4)
	board:draw()
	if board.paused then
		love.graphics.setColor(255,255,255,255)
		love.graphics.print("PAUSED",62,111)
	end
end

function love.update(dt)
	if not board.paused then
		board:update(dt)
		for i,v in ipairs(game.timers) do
			if not v.dead then
				v:update(dt)
			end
		end
		game.collision.update(board)
	end
end

function love.keypressed(key)
	if not board.paused then
		if key == 'space' then
			board.player:shoot()
		elseif key == 'escape' then
			board:pause()
		end
	else
		board:pause()
	end
end

