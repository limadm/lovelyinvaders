function love.conf(t)
	t.title = "Lovely Invaders"
	t.author = "Daniel Lima <danielm@nanohub.tk>"

	-- t.modules.joystick = true
	-- t.modules.audio    = true
	-- t.modules.keyboard = true
	-- t.modules.event    = true
	-- t.modules.image    = true
	-- t.modules.graphics = true
	-- t.modules.timer    = true
	-- t.modules.mouse    = true
	-- t.modules.sound    = true
	-- t.modules.physics  = true

	t.window.height = 480
	t.window.width = 640
	t.window.fullscreen = false
	t.window.vsync = true
	-- t.window.fsaa = 4
end
