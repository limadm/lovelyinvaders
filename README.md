# Lovely Invaders

A little space invaders clone I made for a 2h-tutorial back in 2010, in Lua and Love2D. Features intentionally missing.

Sound effects and graphic sprites by [www.classicgaming.cc](http://www.classicgaming.cc/classics/spaceinvaders/index.php).

---

Use under MIT License (c) 2012 Daniel Lima
