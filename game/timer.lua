module('game',package.seeall)

require 'game.class'

timers = {}

Timer = class()

function Timer:init(t)
	self.time = 0
	self.limit = t
	self.listeners = {}
	table.insert(timers, self)
end

function Timer:addListener(x)
	table.insert(self.listeners, x)
end

function Timer:update(dt)
	local t = self.time
	if t + dt > self.limit then
		for i,v in ipairs(self.listeners) do
			v()
		end
		self.time = 0
	else
		self.time = t + dt
	end
end

function Timer:reset()
	self.time = 0
end

