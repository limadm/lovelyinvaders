module('game',package.seeall)

require 'game.class'
require 'game.collision'
require 'game.resources'

Entity = class()

function Entity:init(sprites, x, y, timer)
	self.sprites = {}
	for i,v in ipairs(sprites) do
		table.insert(self.sprites, game.resources.getImage(v))
	end
	self.sprite = self.sprites[1]
	self.x = x
	self.y = y
	self.w = self.sprite.width
	self.h = self.sprite.height
	self.stance = 1
	if timer then
		timer:addListener(function()
			local stance = self.stance + 1
			if stance > #self.sprites then
				self.stance = 1
			else
				self.stance = stance
			end
			self.sprite = self.sprites[self.stance]
		end)
	end
end

function Entity:draw()
	local s = self.sprite
	if s then
		local w = math.ceil(s.width/2)
		local h = math.ceil(s.height/2)
		love.graphics.draw(s.image, self.x - w, self.y - h)
	end
end

