module('game.resources',package.seeall)

require 'game.class'

local cache = {}

-- Load images, appending image directory prefix and .png suffix
-- Wraps non power of 2 images into power of 2 textures
function getImage(filename)
	local filename = 'img/'..filename..'.png'
	if cache[filename] then
		return cache[filename]
	end
	
	local source = love.image.newImageData(filename)
	local w, h = source:getWidth(), source:getHeight()
	local wrapper = {
		name = filename,
		width = w,
		height = h
	}
	
	-- Find closest power-of-two.
	local wp = math.pow(2, math.ceil(math.log(w)/math.log(2)))
	local hp = math.pow(2, math.ceil(math.log(h)/math.log(2)))
	
	-- Only pad if needed:
	if wp ~= w or hp ~= h then
		local padded = love.image.newImageData(wp, hp)
		padded:paste(source, 0, 0)
		wrapper.image = love.graphics.newImage(padded)
	else
		wrapper.image = love.graphics.newImage(source)
	end
	wrapper.image:setFilter('nearest', 'nearest')
	cache[filename] = wrapper
	return wrapper
end

function getMusic(filename)
	local filename = 'bgm/'..filename..'.mid'
	if not cache[filename] then
		cache[filename] = love.audio.newSource(filename, 'stream')
	end
	return cache[filename]
end

function getSound(filename)
	local filename = 'sfx/'..filename..'.wav'
	if not cache[filename] then
		cache[filename] = love.audio.newSource(filename, 'static')
	end
	return cache[filename]
end

