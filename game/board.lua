module('game',package.seeall)

require 'game.class'
require 'game.entity'
require 'game.player'
require 'game.invader'
require 'game.timer'

Board = class()

function Board:init()
	self.score = 0
	self.paused = true
	self.life = game.resources.getImage('player').image
	
	-- player
	self.player = game.Player({'player'}, 80, 102)
	
	-- shelters
	self.shelters = {}
	for x=1,5 do
		local shelter = game.Entity({'shelter'}, x*29-7, 90)
		table.insert(self.shelters, shelter)
	end
	
	-- invaders
	self.timer = game.Timer(0.5)
	self.invaders = {}
	local spawnline = function (sprites, y, score)
		for x=1,10 do
			local invader = game.Invader(sprites, 0, 0, self.timer, score)
			table.insert(self.invaders, invader)
		end
	end
	spawnline({'invader/1_1', 'invader/1_2'}, 1, 30)
	spawnline({'invader/2_1', 'invader/2_2'}, 2, 20)
	spawnline({'invader/2_1', 'invader/2_2'}, 3, 20)
	spawnline({'invader/3_1', 'invader/3_2'}, 4, 10)
	spawnline({'invader/3_1', 'invader/3_2'}, 5, 10)
	self:resetInvaders()
	self.shotTimer = game.Timer(3)
	self.shotTimer:addListener(function()
		local attacker = self.invaders[math.random(50)]
		if not attacker.dead then attacker:shoot() end
	end)
	
	-- background music
	love.audio.play(game.resources.getMusic('1'))
end

function Board:resetInvaders()
	for i,v in ipairs(self.invaders) do
		local x = (i-1)%10 + 1
		local y = math.ceil(i/10)
		v.x = x*14+3
		v.y = y*10+4
		v.dead = false
	end
	--love.audio.stop()
	--love.audio.play(game.resources.getMusic('1'))
end

function Board:nextLevel()
	self.timer.limit = self.timer.limit * 0.8
	self.shotTimer.limit = self.shotTimer.limit * 0.6
	self:resetInvaders()
	love.audio.rewind()
end

function Board:draw()
	-- lives status bar
	love.graphics.setColor(0,255,0,255)
	love.graphics.line(0,108,160,108)
	for x=1,self.player.lives do
		love.graphics.draw(self.life, x*13-11, 110)
	end
	
	-- player and shelter bases
	for i,shelter in ipairs(self.shelters) do
		shelter:draw()
	end
	self.player:draw()
	
	-- score bar
	love.graphics.setColor(255,255,255,255)
	love.graphics.print("SCORE ",1,1)
	love.graphics.printf(self.score,1,1,70,'right')
	
	-- invaders
	for i,invader in ipairs(self.invaders) do
		invader:draw()
	end
end

function Board:update(dt)
	local invader = self.invaders[1]
	if invader.x > 28 or invader.x < 6 then
		local direction = -invader.direction
		for i,v in ipairs(self.invaders) do
			v.direction = direction
			v.x = v.x + direction
			v.y = v.y + 1
		end
	end
	local kia = 0
	for i,v in ipairs(self.invaders) do
		v:update(dt)
		if v.dead then kia = kia + 1 end
	end
	self.player:update(dt)
	if kia == #self.invaders then
		board:nextLevel()
	end
end

function Board:pause()
	self.paused = not self.paused
end

