require 'game.class'

local Node = class()

function Node:init(list, value)
	self.list = list
	self.value = value
	self.next = nil
	self.prev = nil
end

function Node:remove()
	local list = self.list
	if self.prev then
		self.prev.next = self.next
	else
		list.head = self.next
	end
	if self.next then
		self.next.prev = self.prev
	else
		list.tail = self.prev
	end
	list.size = list.size - 1
end

List = class()

function List:init(...)
	self.head = nil
	self.tail = nil
	self.size = 0
	for i,v in ipairs({...}) do
		self:add(v)
	end
end

function List:add(o)
	local o = Node(self, o)
	if self.head == nil then self.head = o end
	if self.tail then self.tail.next = o end
	o.prev = self.tail
	self.tail = o
	self.size = self.size + 1
end

function List:size(o)
	return self.size
end

function List:nodes()
	local obj = {next=self.head}
	return function()
		obj = obj.next
		return obj
	end
end

function List:values()
	local obj = {next=self.head}
	return function()
		obj = obj.next
		if obj then return obj.value end
	end
end

