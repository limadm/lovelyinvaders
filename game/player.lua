module('game',package.seeall)

require 'game.class'
require 'game.list'
require 'game.shot'
require 'game.entity'
require 'game.timer'
require 'game.collision'

Player = class(game.Entity)

function Player:init(...)
	self._base.init(self, ...)
	self.lives = 3
	self.velocity = 70
	self.shots = List()
	self.rebirth = game.Timer(1)
	self.rebirth:addListener(function()
		if self.dead then
			self.sprite = self.sprites[1]
			self.lives = self.lives - 1
			self.dead = false
		end
	end)
	self.cooldown = game.Timer(0.4)
	self.cooldown:addListener(function()
		self.canShoot = true
	end)
	game.collision.players:add(self)
end

function Player:update(dt)
	if not self.dead then
		local dx = self.velocity * dt
		if love.keyboard.isDown('left') then
			self.x = self.x - dx
		elseif love.keyboard.isDown('right') then
			self.x = self.x + dx
		end
		if self.x < 6 then self.x = 6 end
		if self.x > 155 then self.x = 155 end
	end
	for node in self.shots:nodes() do
		local shot = node.value
		shot:update(dt)
		if shot.y < 10 then shot:die() end
		if shot.dead then node:remove() end
	end
end

function Player:shoot()
	if not self.dead and self.canShoot then
		self.canShoot = false
		self.cooldown:reset()
		local shot = game.Shot({'shot/1'}, self.x, self.y-8, nil, -40)
		self.shots:add(shot)
		game.collision.playerShots:add(shot)
		local sound = game.resources.getSound('shoot')
		love.audio.stop(sound)
		love.audio.play(sound)
	end
end

function Player:draw()
	self._base.draw(self)
	for shot in self.shots:values() do
		shot:draw()
	end
end

function Player:die()
	local sound = game.resources.getSound('explosion')
	love.audio.stop(sound)
	love.audio.play(sound)
	self.sprite = game.resources.getImage('explosion')
	self.rebirth:reset()
	self.dead = true
end

