module('game',package.seeall)

require 'game.class'
require 'game.entity'
require 'game.list'
require 'game.timer'

Invader = class(game.Entity)

function Invader:init(sprites, x, y, timer, score)
	self._base.init(self, sprites, x, y, timer)
	self.score = score
	self.shots = List()
	self.ammo = {{'shot/2_1', 'shot/2_2'},{'shot/3_1', 'shot/3_2'}}
	self.shotTimer = game.Timer(0.15)
	self.direction = 1
	self.dead = false
	if timer then
		timer:addListener(function()
			self.x = self.x + self.direction
			if self.dead then self.sprite = nil end
		end)
	end
	game.collision.invaders:add(self)
end

function Invader:shoot()
	local ammo = self.ammo[math.random(2)]
	local shot = game.Shot(ammo, self.x, self.y+7, self.shotTimer, 20)
	self.shots:add(shot)
	game.collision.invaderShots:add(shot)
end

function Invader:draw()
	self._base.draw(self)
	for shot in self.shots:values() do
		shot:draw()
	end
end

function Invader:update(dt)
	for node in self.shots:nodes() do
		local shot = node.value
		shot:update(dt)
		if shot.y > 104 then shot:die() end
		if shot.dead then node:remove() end
	end
end

function Invader:die()
	self.dead = true
	local sound = game.resources.getSound('invaderkilled')
	love.audio.stop(sound)
	love.audio.play(sound)
	self.sprite = game.resources.getImage('invader/killed')
end

