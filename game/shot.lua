module('game', package.seeall)

require 'game.class'
require 'game.entity'
require 'game.resources'

Shot = class(game.Entity)

function Shot:init(sprites,x,y,timer,velocity)
	self._base.init(self, sprites, x, y, timer)
	self.velocity = velocity
	self.dead = false
end

function Shot:update(dt)
	local dy = self.velocity * dt
	self.y = self.y + dy
end

function Shot:hit(o)
	return math.abs(self.x - o.x) < (self.w + o.w)/2 and math.abs(self.y - o.y) < (self.h + o.h)/2
end

function Shot:die()
	self.dead = true
end
