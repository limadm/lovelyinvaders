module(..., package.seeall)

require 'game.list'

playerShots = List()
invaderShots = List()
players = List()
invaders = List()

function update(board)
	verify(board, playerShots, invaders)
	verify(board, invaderShots, players)
end

function verify(board, shots, targets)
	for shot in shots:values() do
		for target in targets:values() do
			if not shot.dead and not target.dead and shot:hit(target) then
				shot:die()
				target:die()
				if target.score then
					board.score = board.score + target.score
				end
			end
		end
	end
end

